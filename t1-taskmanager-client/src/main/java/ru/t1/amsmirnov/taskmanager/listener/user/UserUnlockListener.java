package ru.t1.amsmirnov.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserUnlockRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserUnlockResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

@Component
public final class UserUnlockListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-unlock";

    @NotNull
    public static final String DESCRIPTION = "Unlock user by login.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@userUnlockListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER USER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken(), login);
        @NotNull final UserUnlockResponse response = userEndpoint.unlockUserByLogin(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
