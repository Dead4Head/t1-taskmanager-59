package ru.t1.amsmirnov.taskmanager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.amsmirnov.taskmanager.api.endpoint.*;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;

@Configuration
@ComponentScan("ru.t1.amsmirnov.taskmanager")
public class ClientConfiguration {

    @Bean
    @NotNull
    public ISystemEndpoint getSystemEndpoint(@NotNull final IPropertyService propertyService) {
        return ISystemEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public IDomainEndpoint getDomainEndpoint(@NotNull final IPropertyService propertyService) {
        return IDomainEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public IProjectEndpoint getProjectEndpoint(@NotNull final IPropertyService propertyService) {
        return IProjectEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public ITaskEndpoint getTaskEndpoint(@NotNull final IPropertyService propertyService) {
        return ITaskEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public IUserEndpoint getUserEndpoint(@NotNull final IPropertyService propertyService) {
        return IUserEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public IAuthEndpoint getAuthEndpoint(@NotNull final IPropertyService propertyService) {
        return IAuthEndpoint.newInstance(propertyService);
    }

}
