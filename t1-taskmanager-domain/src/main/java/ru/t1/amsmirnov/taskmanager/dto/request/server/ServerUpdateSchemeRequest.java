package ru.t1.amsmirnov.taskmanager.dto.request.server;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class ServerUpdateSchemeRequest extends AbstractUserRequest {

    private String scheme = "scheme";

    public ServerUpdateSchemeRequest() {
    }

    public ServerUpdateSchemeRequest(@Nullable String token) {
        super(token);
    }

    public ServerUpdateSchemeRequest(@Nullable String token, @Nullable String scheme) {
        super(token);
        if (scheme == null || !scheme.isEmpty()) return;
        this.scheme = scheme;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

}
