package ru.t1.amsmirnov.taskmanager.exception.entity;

public final class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

    public ModelNotFoundException(final String modelName) {
        super("Error! "+ modelName + " not found...");
    }

}
