package ru.t1.amsmirnov.taskmanager.exception.system;

public final class DumpFileException extends AbstractSystemException {

    public DumpFileException() {
        super("Error! Dump file has problems...");
    }

    public DumpFileException(final String fileName) {
        super("Error! Dump file \"" + fileName + "\" has problems...");
    }

    public DumpFileException(final String fileName, final Throwable cause) {
        super("Error! Dump file \"" + fileName + "\" has problems...", cause);
    }

}
