package ru.t1.amsmirnov.taskmanager.dto.request.server;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class ServerDropSchemeRequest extends AbstractUserRequest {

    public ServerDropSchemeRequest() {
    }

    public ServerDropSchemeRequest(@Nullable String token) {
        super(token);
    }

}
