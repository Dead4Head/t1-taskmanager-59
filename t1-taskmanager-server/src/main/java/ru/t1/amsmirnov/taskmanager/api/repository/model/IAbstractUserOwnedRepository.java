package ru.t1.amsmirnov.taskmanager.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import java.util.List;

public interface IAbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAllSorted(@NotNull String userId, @Nullable String sort);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);

}
