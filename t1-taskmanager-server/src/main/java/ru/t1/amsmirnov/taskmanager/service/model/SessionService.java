package ru.t1.amsmirnov.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.repository.model.ISessionRepository;
import ru.t1.amsmirnov.taskmanager.api.service.model.ISessionService;
import ru.t1.amsmirnov.taskmanager.model.Session;

@Service
public final class SessionService
        extends AbstractUserOwnedModelService<Session, ISessionRepository>
        implements ISessionService {

    @Autowired
    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
    }

}
