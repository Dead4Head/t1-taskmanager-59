package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IAuthEndpoint;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLoginRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLogoutRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLoginResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserLogoutResponse;

import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint() {
        super();
    }

    @NotNull
    @Override
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        try {
            @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
            return new UserLoginResponse(token);
        } catch (@NotNull final Exception e) {
            return new UserLoginResponse(e);
        }
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            authService.logout(session);
            return new UserLogoutResponse();
        } catch (@NotNull final Exception e) {
            return new UserLogoutResponse(e);
        }
    }

}
