package ru.t1.amsmirnov.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IAbstractRepository;
import ru.t1.amsmirnov.taskmanager.api.service.model.IService;
import ru.t1.amsmirnov.taskmanager.comparator.NameComparator;
import ru.t1.amsmirnov.taskmanager.comparator.StatusComparator;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Service
public abstract class AbstractModelService<M extends AbstractModel, R extends IAbstractRepository<M>>
        implements IService<M> {

    @NotNull
    protected R repository;

    public AbstractModelService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> addAll(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        repository.addAll(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        removeAll();
        addAll(models);
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll() throws AbstractException {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws AbstractException {
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        return repository.findAllSorted(getComparator(comparator));
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M update(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.update(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M removeOne(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
        return model;
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    public int getSize() throws AbstractException {
        return findAll().size();
    }

    @Override
    public boolean existById(@NotNull final String id) throws AbstractException {
        try {
            repository.findOneById(id);
            return true;
        } catch (final Exception e) {
            return false;
        }
    }

    protected String getComparator(Comparator<?> comparator) {
        if (comparator instanceof StatusComparator) return "status";
        if (comparator instanceof NameComparator) return "name";
        return "created";
    }

}
