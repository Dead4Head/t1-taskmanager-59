package ru.t1.amsmirnov.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.provider.IBrokerConnectionProvider;

public interface IPropertyService extends IBrokerConnectionProvider {

    @NotNull
    String getMongoDBHost();

    @NotNull
    Integer getMongoDBPort();

    @NotNull
    String getMongoDBName();

    @NotNull
    String getMongoDBDefCollectionName();

}
